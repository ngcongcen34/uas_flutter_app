import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'meal_api_service.dart';
import 'package:uas_flutter_app/meal_detail_page.dart';
import 'package:uas_flutter_app/drawer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'KulinerKu',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MealListPage(),
    );
  }
}

class MealListPage extends StatefulWidget {
  @override
  _MealListPageState createState() => _MealListPageState();
}

class _MealListPageState extends State<MealListPage> {
  late Future<List<dynamic>> _mealsFuture;
  TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _mealsFuture = MealApiService().getMeals('');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meals'),
      ),
      drawer: AppDrawer(),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(16.0),
            child: TextField(
              controller: _searchController,
              decoration: InputDecoration(
                hintText: 'Search meals...',
                suffixIcon: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    setState(() {
                      _mealsFuture =
                          MealApiService().getMeals(_searchController.text);
                    });
                  },
                ),
              ),
            ),
          ),
          Expanded(
            child: Center(
              child: FutureBuilder<List<dynamic>>(
                future: _mealsFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicator();
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    final meals = snapshot.data;
                    return ListView.builder(
                      itemCount: meals!.length,
                      itemBuilder: (context, index) {
                        final meal = meals[index];
                        return ListTile(
                          leading: CachedNetworkImage(
                            imageUrl: meal['strMealThumb'],
                            placeholder: (context, url) =>
                                CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          ),
                          title: Text(meal['strMeal']),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    MealDetailPage(meal: meal),
                              ),
                            );
                          },
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
